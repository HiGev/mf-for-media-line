const gulp = require("gulp");
const sass = require('gulp-sass');
const image = require('gulp-image');
const gulpFont = require('gulp-font');
const watch = require('gulp-watch');
const browserSync = require('browser-sync');



gulp.task('sass', function () {
    return gulp.src('src/styles/main.scss')
      .pipe(sass().on('error', sass.logError))//выводит ошибки если оние есть
      .pipe(gulp.dest('build/styles'));//создаст папки
  });
gulp.task('image', function () {
    return gulp.src('src/img/*')
      .pipe(image())
      .pipe(gulp.dest('build/img'));
  });
gulp.task('font', function () {
    return gulp.src('src/fonts/*')
      .pipe(gulp.dest('build/fonts'));
  });
gulp.task('html', function () {
    return gulp.src('src/pages/*')
      .pipe(gulp.dest('build/'));
  });
gulp.task('browserSync',function(){
    browserSync({
        server:{baseDir:'./build/'},
    })
    });  
gulp.task('watch', function (){
    gulp.watch('src/styles/main.scss',['sass']);
    gulp.watch('src/img/*',['image']);
    gulp.watch('src/fonts/*',['font']);
    gulp.watch('src/pages/*',['html']);
    gulp.watch('src/styles/main.scss').on('change', browserSync.reload);
    gulp.watch('src/img/*').on('change', browserSync.reload);
    gulp.watch('src/fonts/*').on('change', browserSync.reload);
    gulp.watch('src/pages/*').on('change', browserSync.reload);
  });
gulp.task('default',['sass','image','font','html', 'browserSync', 'watch'],function(){
});

